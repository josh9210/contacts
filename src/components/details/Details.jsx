import React from "react";
import "./details.css";

import DetailsForm from "./DetailsForm";
import DetailsNewForm from "./DetailsNewForm";

const Details = ({
  showNewForm,
  detail,
  setNewFormHandler,
  setNewEmailHandler,
  submitNewContact,
  setUpdateDetailHandler,
  setUpdateEmailHandler,
  submitUpdateContact,
}) => {
  return (
    <div className="col-6 p-0">
      {showNewForm ? (
        <>
          <DetailsNewForm
            setNewFormHandler={setNewFormHandler}
            setNewEmailHandler={setNewEmailHandler}
            submitNewContact={submitNewContact}
            showNewForm={showNewForm}
          />
        </>
      ) : detail !== "" ? (
        <DetailsForm
          detail={detail}
          setUpdateDetailHandler={setUpdateDetailHandler}
          setNewEmailHandler={setNewEmailHandler}
          setUpdateEmailHandler={setUpdateEmailHandler}
          submitUpdateContact={submitUpdateContact}
        />
      ) : (
        ""
      )}
    </div>
  );
};

export default Details;
