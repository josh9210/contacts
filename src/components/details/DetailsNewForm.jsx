import React from "react";

import DetailsFormItem from "./DetailsFormItem";

const DetailsNewForm = ({
  setNewFormHandler,
  setNewEmailHandler,
  showNewForm,
  submitNewContact,
}) => {
  return (
    <div className="details-form">
      <DetailsFormItem
        submitNewContact={submitNewContact}
        setNewFormHandler={setNewFormHandler}
        setNewEmailHandler={setNewEmailHandler}
        showNewForm={showNewForm}
      />
    </div>
  );
};

export default DetailsNewForm;
