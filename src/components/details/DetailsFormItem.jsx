import React, { useState } from "react";

import DetailsButtons from "./DetailsButtons";
import DetailsEmail from "./DetailsEmail";

const DetailsFormItem = ({
  setUpdateEmailHandler,
  setNewEmailHandler,
  submitNewContact,
  showNewForm,
  detail,
  setUpdateDetailHandler,
  setNewFormHandler,
  submitUpdateContact,
}) => {
  const [showNewEmail, setShowNewEmail] = useState(false);

  return (
    <>
      <form
        onSubmit={
          detail === undefined
            ? (e) => submitNewContact(e)
            : (e) => submitUpdateContact(e, detail.id)
        }
      >
        <div className="details-form-item-wrapper">
          <div className="details-form-item">
            <label htmlFor="firstName">First Name</label>
            <input
              type="text"
              id="firstName"
              name="firstName"
              key={detail === undefined ? "" : detail.id}
              defaultValue={detail === undefined ? "" : detail.firstName}
              onChange={
                detail === undefined
                  ? (e) => setNewFormHandler(e)
                  : (e) => setUpdateDetailHandler(e)
              }
              required
            />
          </div>
          <div className="details-form-item">
            <label htmlFor="lastName">Last Name</label>
            <input
              type="text"
              id="lastName"
              name="lastName"
              key={detail === undefined ? "" : detail.id}
              defaultValue={detail === undefined ? "" : detail.lastName}
              onChange={
                detail === undefined
                  ? (e) => setNewFormHandler(e)
                  : (e) => setUpdateDetailHandler(e)
              }
              required
            />
          </div>
        </div>

        <br />

        <div className="details-form-item-email">
          <div>
            <span>Email</span>
            <div>
              {detail !== undefined
                ? detail.emails.map((email, i) => (
                    <DetailsEmail key={i} email={email} />
                  ))
                : ""}
            </div>
          </div>
        </div>
        {showNewEmail ? (
          <div>
            <div className="details-form-item">
              <input
                type="email"
                id="emails"
                name="emails"
                onChange={
                  detail === undefined
                    ? (e) => setNewEmailHandler(e)
                    : (e) => setUpdateEmailHandler(e)
                }
              />
            </div>
          </div>
        ) : (
          ""
        )}
        <DetailsButtons showNewForm={showNewForm} detail={detail} />
      </form>
      <div
        className="details-form-item-add-email"
        onClick={() => setShowNewEmail(!showNewEmail)}
      >
        <i className="fas fa-plus-circle"></i>
        <span style={{ margin: "0 0 0 0.45em" }}>add email</span>
      </div>
    </>
  );
};

export default DetailsFormItem;
