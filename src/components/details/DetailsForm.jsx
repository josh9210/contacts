import React from "react";

import DetailsFormItem from "./DetailsFormItem";

const DetailsForm = ({
  detail,
  setNewFormHandler,
  setUpdateDetailHandler,
  setNewEmailHandler,
  setUpdateEmailHandler,
  submitUpdateContact,
}) => {
  return (
    <div className="details-form">
      {detail !== "" ? (
        <DetailsFormItem
          detail={detail}
          setUpdateDetailHandler={setUpdateDetailHandler}
          setUpdateEmailHandler={setUpdateEmailHandler}
          submitUpdateContact={submitUpdateContact}
        />
      ) : (
        <DetailsFormItem
          detail={detail}
          setNewFormHandler={setNewFormHandler}
          setNewEmailHandler={setNewEmailHandler}
        />
      )}
    </div>
  );
};

export default DetailsForm;
