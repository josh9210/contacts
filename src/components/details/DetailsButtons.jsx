import React from "react";
import { deleteContact } from "../../service";

const DetailsButtons = ({ showNewForm, detail }) => {
  const deleteContactHandler = (id) => {
    deleteContact(id)
      .then((res) => {
        alert(`${id} Deleted.`);
        window.location.reload();
      })
      .catch((err) => console.log(err));
  };
  return (
    <div className="details-buttons">
      {!showNewForm ? (
        <div>
          <button
            className="btn-lg btn-danger"
            onClick={() => deleteContactHandler(detail.id)}
          >
            Delete
          </button>
        </div>
      ) : (
        <div></div>
      )}
      <div>
        <button
          className="btn-lg btn-secondary mr-4"
          onClick={() => window.location.reload()}
        >
          Cancel
        </button>
        <input className="btn-lg btn-primary" type="submit" value="Save" />
      </div>
    </div>
  );
};

export default DetailsButtons;
