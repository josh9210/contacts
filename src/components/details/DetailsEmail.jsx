import React, { useState } from "react";

const DetailsEmail = ({ email }) => {
  const [showDelete, setShowDelete] = useState(false);

  const showDeleteHandler = () => {
    setShowDelete(true);
  };
  const hideDeleteHandler = () => {
    setShowDelete(false);
  };

  return (
    <div
      className="detail-email"
      onMouseOver={showDeleteHandler}
      onMouseOut={hideDeleteHandler}
    >
      <h5 style={{ margin: "0 0.45em 0 0" }}>{email}</h5>
      {showDelete && (
        <i
          className="fas fa-minus-circle"
          style={{ color: "red", fontSize: "24px", cursor: "pointer" }}
        ></i>
      )}
    </div>
  );
};

export default DetailsEmail;
