import React from "react";

const ContactsHeader = ({ showNewFormHandler }) => {
  return (
    <div className="contact-header ">
      <h1>Contacts</h1>
      <i
        className="fas fa-plus-circle"
        style={{
          fontSize: "2.75em",
          color: "cornflowerblue",
          cursor: "pointer",
        }}
        onClick={showNewFormHandler}
      ></i>
    </div>
  );
};

export default ContactsHeader;
