import React, { useState, useEffect } from "react";
import "./contacts.css";
import { getAllContacts } from "../../service";

import ContactsItem from "./ContactsItem";
import ContactsHeader from "./ContactsHeader";

const Contacts = ({ showNewFormHandler, showContactDetail }) => {
  const [contacts, setContacts] = useState([]);
  useEffect(() => {
    getAllContacts()
      .then((res) => setContacts(res.data))
      .catch((err) => console.log(err));
  }, []);

  return (
    <div className="contacts col-2 p-0">
      <ContactsHeader showNewFormHandler={showNewFormHandler} />
      <div className="contact-list">
        {contacts.map((contact, i) => (
          <ContactsItem
            key={i}
            contact={contact}
            showContactDetail={showContactDetail}
          />
        ))}
      </div>
    </div>
  );
};

export default Contacts;
