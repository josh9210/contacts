import React from "react";

const ContactsItem = ({ contact, showContactDetail }) => {
  return (
    <div className="contact-item" onClick={() => showContactDetail(contact.id)}>
      {contact.firstName} {contact.lastName}
    </div>
  );
};

export default ContactsItem;
