import React, { useState } from "react";
import "./App.css";
import { getContactDetail, createNewContact, updateContact } from "./service";

import Contacts from "./components/contacts/Contacts";
import Details from "./components/details/Details";

const App = () => {
  const [showNewForm, setShowNewForm] = useState(false);
  const [detail, setDetail] = useState("");
  const [newForm, setNewForm] = useState({
    firstName: "",
    lastName: "",
  });
  const [newEmail, setNewEmail] = useState([]);

  const showNewFormHandler = () => {
    setShowNewForm(true);
  };
  const showContactDetail = (id) => {
    setShowNewForm(false);
    getContactDetail(id)
      .then((res) => setDetail(res.data))
      .catch((err) => console.log(err));
  };

  const setNewFormHandler = (e) => {
    const { name, value } = e.target;
    setNewForm((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };
  const setNewEmailHandler = (e) => {
    const { value } = e.target;
    setNewEmail([value]);
  };

  const submitNewContact = (e) => {
    e.preventDefault();
    const data = {
      firstName: newForm.firstName,
      lastName: newForm.lastName,
      emails: newEmail,
    };
    createNewContact(data)
      .then((res) => {
        alert("New contact added!");
        window.location.reload();
      })
      .catch((err) => console.log(err));
  };

  const submitUpdateContact = (e, id) => {
    e.preventDefault();
    const data = {
      firstName: detail.firstName,
      lastName: detail.lastName,
      emails: newEmail.length === 0 ? detail.emails : newEmail,
    };
    updateContact(id, data)
      .then((res) => {
        alert("Contact updated!");
        window.location.reload();
      })
      .catch((err) => console.log(err));
  };

  const setUpdateDetailHandler = (e) => {
    const { name, value } = e.target;
    setDetail((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  const setUpdateEmailHandler = (e) => {
    const { value } = e.target;
    let oldValue = detail.emails;
    setNewEmail([...oldValue, value]);
  };
  return (
    <div className="row">
      <Contacts
        showNewFormHandler={showNewFormHandler}
        showContactDetail={showContactDetail}
      />
      <Details
        showNewForm={showNewForm}
        detail={detail}
        setNewFormHandler={setNewFormHandler}
        setNewEmailHandler={setNewEmailHandler}
        submitNewContact={submitNewContact}
        setUpdateDetailHandler={setUpdateDetailHandler}
        setUpdateEmailHandler={setUpdateEmailHandler}
        submitUpdateContact={submitUpdateContact}
      />
    </div>
  );
};

export default App;
