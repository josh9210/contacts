import axios from "axios";

const API_URL = "https://avb-contacts-api.herokuapp.com/contacts";

export const getAllContacts = () => {
  return axios.get(API_URL);
};

export const getContactDetail = (id) => {
  return axios.get(`${API_URL}/${id}`);
};

export const createNewContact = (data) => {
  return axios.post(API_URL, data);
};

export const updateContact = (id, data) => {
  return axios.put(`${API_URL}/${id}`, data);
};

export const deleteContact = (id) => {
  return axios.delete(`${API_URL}/${id}`);
};
